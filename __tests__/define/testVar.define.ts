export const TEST_URL = {
  google: 'https://google.com',
  yandex: 'https://yandex.com',
  habr: 'https://habr.com',
  bing: 'https://bing.com',
  duckduckgo: 'https://duckduckgo.com',
};

export const VALID_LINKS_WITHOUT_PROTOCOL = [
  'google.com',
  'ya.ru',
  'bing.com',
  'nohello.net',
];

export const VALID_LINKS: string[] = [
  ...Object.values(TEST_URL),
  ...VALID_LINKS_WITHOUT_PROTOCOL,
];

export const INVALID_LINKS: string[] = [
  'go.mooooooc',
  'htt://google.com',
  'www.https://.com/123',
  'www.www.www.www',
  'ya.ru.',
  'teodoro.teodoro1.1',
  '/localhost/',
  '127,0,0,1',
  '',
  '1',
];

export const WEBPACK_HEAD = `
<head>
    <title>webpack</title>
    <meta data-rh="true" charset="utf-8">
    <meta data-rh="true" name="theme-color" content="#2B3A42">
    <meta data-rh="true" name="viewport" content="width=device-width, initial-scale=1">
    <meta data-rh="true" name="description" content="webpack is a module bundler. Its main purpose is to bundle JavaScript files for usage in a browser, yet it is also capable of transforming, bundling, or packaging just about any resource or asset.">
    <meta data-rh="true" property="og:site_name" content="webpack">
    <meta data-rh="true" property="og:type" content="website">
    <meta data-rh="true" property="og:title" content="webpack">
    <meta data-rh="true" property="og:description" name="description" content="webpack is a module bundler. Its main purpose is to bundle JavaScript files for usage in a browser, yet it is also capable of transforming, bundling, or packaging just about any resource or asset.">
    <meta data-rh="true" property="og:image" content="https://webpack.js.org/icon-pwa-512x512.d3dae4189855b3a72ff9.png">
    <meta data-rh="true" property="twitter:card" content="summary">
    <meta data-rh="true" property="twitter:site" content="@webpack">
    <meta data-rh="true" property="twitter:creator" content="@webpack">
    <meta data-rh="true" property="twitter:domain" content="https://webpack.js.org/">
    <meta data-rh="true" name="mobile-web-app-capable" content="yes">
    <meta data-rh="true" name="apple-mobile-web-app-capable" content="yes">
    <meta data-rh="true" name="apple-mobile-web-app-status-bar-style" content="black">
    <meta data-rh="true" name="apple-mobile-web-app-title" content="webpack">
    <meta data-rh="true" name="msapplication-TileImage" content="/icon_150x150.png">
    <meta data-rh="true" name="msapplication-TileColor" content="#465e69">
    <link data-rh="true" rel="icon" type="image/x-icon" href="/favicon.f326220248556af65f41.ico">
    <link data-rh="true" rel="manifest" href="/manifest.json">
    <link data-rh="true" rel="canonical" href="https://webpack.js.org/">
    <link data-rh="true" rel="icon" sizes="192x192" href="/icon_192x192.png">
    <link data-rh="true" rel="icon" sizes="512x512" href="/icon_512x512.png">
    <link data-rh="true" rel="apple-touch-icon" href="/icon_180x180.png">
    <link data-rh="true" rel="mask-icon" href="/logo-on-white-bg.47eff95f9c01c5972f6f.svg" color="#465e69">
    <link rel="stylesheet" href="/index.09b23fb7353e3734e674.css">
    <link rel="stylesheet" href="/7677.4feb75340ce362ace7a4.css">
    <script async="" type="text/javascript" src="//www.google-analytics.com/analytics.js"></script>
    <link rel="stylesheet" type="text/css" href="/9701.f8c7a0c99081af9c8952.css">
</head>
`;

export const WEBPACK_URL = new URL('https://webpack.js.org/');

export const WEBPACK_HEAD_OBJECT = {
  title: 'webpack',
  theme: '#2B3A42',
  siteName: 'webpack',
  locale: null,
  appId: null,
  type: 'website',
  iconList: [
    WEBPACK_URL.origin + '/favicon.f326220248556af65f41.ico',
    WEBPACK_URL.origin + '/icon_192x192.png',
    WEBPACK_URL.origin + '/icon_512x512.png',
    WEBPACK_URL.origin + '/icon_180x180.png',
    WEBPACK_URL.origin + '/icon_150x150.png',
  ],
  previewList: [
    'https://webpack.js.org/icon-pwa-512x512.d3dae4189855b3a72ff9.png',
  ],
  descriptionList: [
    'webpack is a module bundler. Its main purpose is to bundle JavaScript files for usage in a browser, yet it is also capable of transforming, bundling, or packaging just about any resource or asset.',
    'webpack is a module bundler. Its main purpose is to bundle JavaScript files for usage in a browser, yet it is also capable of transforming, bundling, or packaging just about any resource or asset.',
    'webpack is a module bundler. Its main purpose is to bundle JavaScript files for usage in a browser, yet it is also capable of transforming, bundling, or packaging just about any resource or asset.',
  ],
  encoding: 'utf-8',
};
