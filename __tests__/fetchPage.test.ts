import test from 'ava';
import fetchPage from '../lib/fetchPage.js';
import { TEST_URL } from './define/testVar.define.js';

for (const site in TEST_URL) {
  const currentSite = TEST_URL[site as keyof typeof TEST_URL];
  const title = `${site[0].toUpperCase() + site.slice(1)}: ${currentSite}`;
  const url = new URL(currentSite);

  test(`Getting page [${title}]`, async t => {
    await fetchPage(url)
      .then(pageContent => {
        t.true(pageContent.length > 0, 'Content is empty!');
      })
      .catch(() => t.fail("Page isn't downloaded! " + url));
  });
}
