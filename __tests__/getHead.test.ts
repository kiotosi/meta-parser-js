import test from 'ava';
import { TEST_URL } from './define/testVar.define.js';
import { BASIC_PAIR } from './define/mocks.define.js';
import getHead from '../lib/getHead.js';
import fetchPage from '../lib/fetchPage.js';

/**
 * Get HTML of site
 * @param siteName Name of the site
 * @returns HTML of the page
 */
function getPage(siteName: string): Promise<string> {
  const currentURL = TEST_URL[siteName as keyof typeof TEST_URL];
  const url = new URL(currentURL);
  return fetchPage(url);
}

test('Valid HTML', t => {
  t.true(getHead(BASIC_PAIR.valid) !== null, 'Valid head is null!');
});

test('Invalid HTML', t => {
  t.false(getHead(BASIC_PAIR.invalid) !== null, "Invalid head isn't null!");
});

for (const site in TEST_URL) {
  const title = site[0].toUpperCase() + site.slice(1);

  test(`Getting head for ${title}`, async t => {
    const page = await getPage(site);
    const head = getHead(page);
    t.true(head !== null, `Head didn't parsed!\n\tHead: ${head}`);
  });
}
