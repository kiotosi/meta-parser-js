import {
  WEBPACK_HEAD,
  WEBPACK_HEAD_OBJECT,
  WEBPACK_URL,
} from './define/testVar.define.js';
import parseHead from '../lib/getHead.js';
import MetaParser from '../lib/parsers/metaParser.js';
import test from 'ava';

let metaParser: MetaParser;

test.before(() => {
  const head = parseHead(WEBPACK_HEAD);

  if (!head) {
    return;
  }

  metaParser = new MetaParser(head, WEBPACK_URL);
});

test('URL', t => {
  t.is(metaParser.url.origin, WEBPACK_URL.origin);
});

test('Title', t => {
  t.is(metaParser.title, WEBPACK_HEAD_OBJECT.title);
});

test('Type', t => {
  t.is(metaParser.type, WEBPACK_HEAD_OBJECT.type);
});

test('Theme', t => {
  t.is(metaParser.themeColor, WEBPACK_HEAD_OBJECT.theme);
});

test('Locale', t => {
  t.is(metaParser.locale, WEBPACK_HEAD_OBJECT.locale);
});

test('App ID', t => {
  t.is(metaParser.appId, WEBPACK_HEAD_OBJECT.locale);
});

test('Site Name', t => {
  t.is(metaParser.siteName, WEBPACK_HEAD_OBJECT.siteName);
});

test('Encoding', t => {
  t.is(metaParser.encoding, WEBPACK_HEAD_OBJECT.encoding);
});

test('Description', t => {
  t.deepEqual(metaParser.descriptionList, WEBPACK_HEAD_OBJECT.descriptionList);
});

test('Icon', t => {
  t.deepEqual(metaParser.iconList, WEBPACK_HEAD_OBJECT.iconList);
});

test('Preview', t => {
  t.deepEqual(metaParser.previewList, WEBPACK_HEAD_OBJECT.previewList);
});
