import test from 'ava';
import {
  VALID_LINKS,
  VALID_LINKS_WITHOUT_PROTOCOL,
} from './define/testVar.define.js';
import parseLink from '../lib/parseLink.js';

test('Links without protocol', t => {
  for (const link of VALID_LINKS_WITHOUT_PROTOCOL) {
    t.true(parseLink(link) instanceof URL, 'There is invalid link! ' + link);
  }
});

test('Full links', t => {
  for (const link of VALID_LINKS) {
    t.true(parseLink(link) instanceof URL, 'There is invalid link! ' + link);
  }
});
