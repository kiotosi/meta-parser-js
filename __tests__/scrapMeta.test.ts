import test from 'ava';
import scrapMeta from '../index.js';
import { TEST_URL } from './define/testVar.define.js';

for (const site in TEST_URL) {
  const currentSite = TEST_URL[site as keyof typeof TEST_URL];
  const title = `${site[0].toUpperCase() + site.slice(1)}: ${currentSite}`;
  const url = new URL(currentSite);

  test(`Scrapping meta from [${title}]`, async t => {
    await scrapMeta(url)
      .then(meta => {
        t.deepEqual(
          Object.keys(meta.info),
          [
            'title',
            'url',
            'descriptionList',
            'iconList',
            'previewList',
            'themeColor',
            'locale',
            'siteName',
            'appId',
            'type',
            'encoding',
          ],
          'Content is empty!'
        );
      })
      .catch(() => t.fail("Page isn't downloaded! " + url));
  });
}
