export const HTTPS_REGEX = /^http(s)?:\/\//giu;

export const HEAD_TAG_REGEX = {
  start: /\s*<head[\s\w\W]*>\s*/giu,
  end: /\s*<\/head>\s*/giu,
};

export const DOCTYPE_TAG_REGEX = /^<!doctype[\s\w\d]*>/i;
