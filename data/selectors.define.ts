import { MetaPair } from '../typings/metaParser.interface.js';

// Favicon selectors
export const ICON_SELECTORS: MetaPair[] = [
  {
    hook: 'link[rel=icon]',
    contentProperty: 'href',
  },
  {
    hook: 'link[rel=apple-touch-icon]',
    contentProperty: 'href',
  },
  {
    hook: 'link[rel="shortcut icon"]',
    contentProperty: 'href',
  },
  {
    hook: 'meta[name="msapplication-TileImage"]',
    contentProperty: 'content',
  },
];

// Description selectors
export const DESCRIPTION_SELECTORS: MetaPair[] = [
  {
    hook: 'meta[name=description]',
    contentProperty: 'content',
  },
  {
    hook: 'meta[property="og:description"]',
    contentProperty: 'content',
  },
];

// Preview selectors
export const PREVIEW_SELECTORS: MetaPair[] = [
  {
    hook: 'link[rel=image_src]',
    contentProperty: 'href',
  },
  {
    hook: 'meta[itemprop=image]',
    contentProperty: 'content',
  },
  {
    hook: 'meta[property="og:image"]',
    contentProperty: 'content',
  },
  {
    hook: 'meta[name="twitter:image:src"]',
    contentProperty: 'content',
  },
];
