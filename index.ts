import fetchPage from './lib/fetchPage.js';
import getHead from './lib/getHead.js';
import MetaParser from './lib/parsers/metaParser.js';
import parseLink from './lib/parseLink.js';

/**
 * Parse meta-tags from site
 * @param url URL to parse
 * @returns Meta-tag parser
 */
export default async function scrapMeta(
  url: string | URL
): Promise<MetaParser> {
  const link = parseLink(url);

  if (link === null) {
    const err = new Error("Can't parse the link!\n" + url);
    throw err;
  }

  const html = await fetchPage(link);
  const head = getHead(html);

  if (!head) {
    throw new Error("Can't parse head tag in site!");
  }

  return new MetaParser(head, link);
}
