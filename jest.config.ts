import { Config } from 'jest';

const EXTENSIONS = ['js', 'mjs', 'ts'];

const config: Config = {
  moduleFileExtensions: EXTENSIONS,
  extensionsToTreatAsEsm: ['.ts'],
  preset: 'ts-jest',
  testRegex: 'w*.test.ts$',
  testEnvironment: 'jest-environment-node',
  verbose: true,
  transform: {},
};

export default config;
