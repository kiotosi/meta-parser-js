import a from 'axios';

/**
 * Fetch site
 * @param {string | URL} link Link to the site
 * @returns {Promise<string>} Site data
 */
export default async function fetchPage(link: URL): Promise<string> {
  // Get site
  const response = await a.get<string>(link.href, {
    headers: {
      'User-Agent':
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    },
  });

  // Check status code
  if (response.status !== 200) {
    console.warn('Something wrong with site: ' + link.origin);
    console.info('Status code: ', response.status);
    return Promise.reject('Wrong status code (not 200): ' + response.status);
  }

  // Get HTML
  return response.data;
}
