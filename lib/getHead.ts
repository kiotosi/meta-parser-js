import { parse as parseHTML, HTMLElement } from 'node-html-parser';
import { DOCTYPE_TAG_REGEX, HEAD_TAG_REGEX } from '../data/regex.define.js';

/**
 * Slice <head> out of <body>
 * @param html Full-page HTML
 * @returns Head of the page
 */
export default function getHead(html: string): HTMLElement | null {
  const start = html.search(HEAD_TAG_REGEX.start);
  const end = html.search(HEAD_TAG_REGEX.end);

  // If there's no <head> tag
  if ([start, end].includes(-1)) {
    console.warn('There is no head tag!');
    console.info('[Fallback] Trying to fetch <doctype>');
    return getFallbackHTML(html);
  }

  const parser = parseHTML(html);
  return parser.querySelector('head');
}

function getFallbackHTML(html: string): HTMLElement | null {
  console.info('Trying to get fallback HTML');

  // Check if there is <!doctype ...>
  if (html.search(DOCTYPE_TAG_REGEX) === -1) {
    console.error("Can't parse the site");
    return null;
  }

  return parseHTML(html);
}
