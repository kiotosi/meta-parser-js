import { isURLFull } from './utils.js';
/**
 * Parse link from string to URL
 * @param {string | URL} url URL of site
 * @returns {URL} Constructed URL object
 */
export default function parseLink(url: string | URL): URL | null {
  if (url instanceof URL) {
    return url;
  }

  // If there's no protocol
  if (!isURLFull(url)) {
    url = 'https://' + url;
  }

  try {
    const constructedURL = new URL(url);
    return constructedURL;
  } catch (e) {
    console.error(e);
    console.warn('Incorrect link');
    return null;
  }
}
