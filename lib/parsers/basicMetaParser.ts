import Manifest from '../../typings/manifest.interface.js';
import { isURLFull } from '../utils.js';
import { HTMLElement } from 'node-html-parser';
import a from 'axios';

export default class BasicMetaTagParser {
  constructor(protected _currentHead: HTMLElement, public url: URL) {}

  /**
   * Get title
   */
  get title(): string {
    return this._currentHead.querySelector('title')?.innerText ?? '';
  }

  /**
   * Fetch manifest.json file of the site
   * @returns manifest.json
   */
  public async fetchManifest(): Promise<Manifest> {
    const manifestURL = this._fetchManifestURL();

    if (manifestURL) {
      const response = await a.get<Manifest>(manifestURL);
      return response.data;
    }

    return Promise.reject(
      "There's no manifest.json in the site " + this.url.origin
    );
  }

  /**
   * Fetch manifest url from meta-tag
   */
  private _fetchManifestURL(): string | null {
    const manifestElement =
      this._currentHead.querySelector('link[rel=manifest]');
    const manifestURL = manifestElement?.getAttribute('href');

    if (manifestURL && manifestURL !== '') {
      return isURLFull(manifestURL)
        ? manifestURL
        : this.url.origin + manifestURL;
    }

    return null;
  }

  /**
   * Concatenate site URL to content URL
   * @param content Link from element content
   * @returns Full URL for the link
   */
  protected _concatenateLink(content: string) {
    if (!isURLFull(content)) {
      content = this.url.origin + content;
    }

    return content;
  }
}
