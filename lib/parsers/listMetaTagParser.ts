import { ListMetaType, MetaPair } from '../../typings/metaParser.interface.js';
import {
  ICON_SELECTORS,
  DESCRIPTION_SELECTORS,
  PREVIEW_SELECTORS,
} from '../../data/selectors.define.js';
import { HTMLElement } from 'node-html-parser';
import BasicMetaTagParser from './basicMetaParser.js';

export default class ListMetaTagParser extends BasicMetaTagParser {
  constructor(head: HTMLElement, url: URL) {
    super(head, url);
  }

  /**
   * Get content for the list of selectors
   * @param isLink - Is content of the node is link
   * @param type - MetaType of node list
   * @returns Content list of the selectors
   */
  private _getContentList({
    isLink,
    type,
  }: {
    isLink: boolean;
    type: ListMetaType;
  }): string[] {
    const contentList: string[] = [];
    let selectorList: MetaPair[];

    switch (type) {
      case ListMetaType.descriptionList:
        selectorList = DESCRIPTION_SELECTORS;
        break;
      case ListMetaType.iconList:
        selectorList = ICON_SELECTORS;
        break;
      default:
        selectorList = PREVIEW_SELECTORS;
    }

    // Go through each selector
    for (const selectorPair of selectorList) {
      // Get list of elements
      const nodeList = this._currentHead.querySelectorAll(selectorPair.hook);

      // If there is no elements
      if (nodeList.length <= 0) {
        continue;
      }

      // Go through each node
      nodeList.forEach(node => {
        // Content of the node
        const nodeContent = node.getAttribute(selectorPair.contentProperty);

        // If there's one or more node
        if (node && nodeContent) {
          contentList.push(
            isLink ? this._concatenateLink(nodeContent) : nodeContent
          );
        }
      });
    }

    return contentList;
  }

  /**
   * Get favicons list
   */
  get iconList(): string[] {
    return this._getContentList({ isLink: true, type: ListMetaType.iconList });
  }

  /**
   * Get description list
   */
  get descriptionList(): string[] {
    return this._getContentList({
      isLink: false,
      type: ListMetaType.descriptionList,
    });
  }

  /**
   * Get preview list
   */
  get previewList(): string[] {
    return this._getContentList({
      isLink: true,
      type: ListMetaType.previewList,
    });
  }
}
