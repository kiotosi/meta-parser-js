import { HTMLElement } from 'node-html-parser';
import { OptionalMeta } from '../../typings/metaParser.interface.js';
import { MetaScrapperInfo } from '../../typings/metaParser.interface.js';
import ListMetaTagParser from './listMetaTagParser.js';
import UniqueMetaTagParser from './uniqueMetaTagParser.js';
import BasicMetaTagParser from './basicMetaParser.js';

export default class MetaParser
  extends BasicMetaTagParser
  implements MetaScrapperInfo
{
  private _uniqueParser: UniqueMetaTagParser;
  private _listParser: ListMetaTagParser;

  /**
   * Parser for meta tags
   * @param head Stringified <head> HTML
   * @param url URL of site
   */
  constructor(head: HTMLElement, url: URL) {
    super(head, url);

    // Composition
    this._uniqueParser = new UniqueMetaTagParser(head, url);
    this._listParser = new ListMetaTagParser(head, url);
  }

  // Getters
  get iconList(): string[] {
    return this._listParser.iconList;
  }

  get descriptionList(): string[] {
    return this._listParser.descriptionList;
  }

  get previewList(): string[] {
    return this._listParser.previewList;
  }

  get themeColor(): OptionalMeta {
    return this._uniqueParser.themeColor;
  }

  get type(): OptionalMeta {
    return this._uniqueParser.type;
  }

  get locale(): OptionalMeta {
    return this._uniqueParser.locale;
  }

  get siteName(): OptionalMeta {
    return this._uniqueParser.siteName;
  }

  get appId(): OptionalMeta {
    return this._uniqueParser.appId;
  }

  get encoding(): OptionalMeta {
    return this._uniqueParser.encoding;
  }

  /**
   * Get all info, except manifest
   */
  get info(): MetaScrapperInfo {
    return {
      title: this.title,
      url: this.url,
      descriptionList: this.descriptionList,
      iconList: this.iconList,
      previewList: this.previewList,
      themeColor: this.themeColor,
      locale: this.locale,
      siteName: this.siteName,
      appId: this.appId,
      type: this.type,
      encoding: this.encoding,
    };
  }
}
