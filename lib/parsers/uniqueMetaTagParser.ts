import { MetaType } from '../../typings/metaParser.interface.js';
import { HTMLElement } from 'node-html-parser';
import BasicMetaTagParser from './basicMetaParser.js';

/**
 * Parser for unique tags in DOM
 */
export default class UniqueMetaTagParser extends BasicMetaTagParser {
  constructor(head: HTMLElement, url: URL) {
    super(head, url);
  }

  /**
   * Get theme color
   */
  get themeColor(): string | null {
    return this._getSimpleMeta(MetaType['theme']);
  }

  /**
   * Get og:type
   */
  get type(): string | null {
    return this._getSimpleMeta(MetaType['type']);
  }

  get encoding(): string | null {
    return this._getSimpleMeta(MetaType['encoding']);
  }

  /**
   * Get fb:app_id
   */
  get appId(): string | null {
    return this._getSimpleMeta(MetaType['app_id']);
  }

  /**
   * Get og:locale
   */
  get locale(): string | null {
    return this._getSimpleMeta(MetaType['locale']);
  }

  /**
   * Get og:site_name
   */
  get siteName(): string | null {
    return this._getSimpleMeta(MetaType['site_name']);
  }

  /**
   * Get content of meta tag
   * @param {MetaType} type - Type of meta
   */
  private _getSimpleMeta(type: MetaType): string | null {
    let metaElement: HTMLElement | null;

    switch (type) {
      case MetaType['encoding']:
        metaElement = this._currentHead.querySelector('meta[charset]');
        return metaElement?.getAttribute('charset') ?? null;

      case MetaType['theme']:
        metaElement = this._currentHead.querySelector('meta[name=theme-color]');
        break;

      case MetaType['app_id']:
        metaElement = this._currentHead.querySelector(
          'meta[propertyrel="fb:app_id"]'
        );
        break;

      case MetaType['locale']:
        metaElement = this._currentHead.querySelector(
          'meta[property="og:locale"]'
        );
        break;

      case MetaType['site_name']:
        metaElement = this._currentHead.querySelector(
          'meta[property="og:site_name"]'
        );
        break;

      case MetaType['type']:
        metaElement = this._currentHead.querySelector(
          'meta[property="og:type"]'
        );
        break;

      default:
        return null;
    }

    return metaElement?.getAttribute('content') ?? null;
  }
}
