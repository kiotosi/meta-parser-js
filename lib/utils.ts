import { HTTPS_REGEX } from '../data/regex.define.js';

/**
 * Check if URL is full
 * @param url URL of the site
 * @returns Is URL have https://
 */
export function isURLFull(url: string): boolean {
  // Update search index
  clearLastRegexIndex(HTTPS_REGEX);

  // Test regex
  return HTTPS_REGEX.test(url);
}

function clearLastRegexIndex(reg: RegExp) {
  reg.lastIndex = -1;
}
