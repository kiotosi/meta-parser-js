type ManifestIcons = {
  src: string;
  sizes: string;
  type: string;
};

type ManifestRelatedApplications = {
  platform: string;
  url: string;
};

export default interface Manifest {
  name: string;
  short_name: string;
  start_url: string;
  icons?: ManifestIcons;
  display?: string;
  background_color?: string;
  description?: string;
  related_applications?: ManifestRelatedApplications[];
  author?: string;
  background?: string;
  default_locale?: string;
  developer?: string;
  homepage_url?: string;
  offline_enabled?: string;
  theme?: string;
}
