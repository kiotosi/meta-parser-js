export type OptionalMeta = string | null;

export interface MetaScrapperInfo {
  title: string;
  iconList: string[];
  descriptionList: string[];
  previewList: string[];
  url: URL;
  themeColor: OptionalMeta;
  type: OptionalMeta;
  locale: OptionalMeta;
  siteName: OptionalMeta;
  appId: OptionalMeta;
  encoding: OptionalMeta;
}

export enum MetaType {
  'theme',
  'preview',
  'type',
  'locale',
  'app_id',
  'site_name',
  'encoding',
}

export enum ListMetaType {
  'iconList',
  'descriptionList',
  'previewList',
}

export interface MetaPair {
  hook: string;
  contentProperty: string;
}
